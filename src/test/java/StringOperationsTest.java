import org.testng.Assert;
import org.testng.annotations.Test;

public class StringOperationsTest {

  // tests for method reverse

  @Test
  public void testReverse_tc1() throws Exception {
    String input = "agh";
    String ExpReversed = "hga";

    Assert.assertEquals ( StringOperations.reverse( input ), ExpReversed );
  }

  @Test
  public void testReverse_tc2() throws Exception {
    String input = "długie słowo";
    String ExpReversed = "owołs eigułd";

    Assert.assertEquals ( StringOperations.reverse( input ), ExpReversed );
  }

  // tests for method reverse2

  @Test
  public void testReverse2_tc1() throws Exception {
    String input = "agh";
    String ExpReversed = "hga";

    Assert.assertEquals ( StringOperations.reverse2( input ), ExpReversed );
  }

  @Test
  public void testReverse2_tc2() throws Exception {
    String input = "długie słowo";
    String ExpReversed = "owołs eigułd";

    Assert.assertEquals ( StringOperations.reverse2( input ), ExpReversed );
  }

  // tests for method reverse3

  @Test
  public void testReverse3_tc1() throws Exception {
    String input = "agh";
    String ExpReversed = "hga";

    Assert.assertEquals ( StringOperations.reverse3( input ), ExpReversed );
  }

  @Test
  public void testReverse3_tc2() throws Exception {
    String input = "długie słowo";
    String ExpReversed = "owołs eigułd";

    Assert.assertEquals ( StringOperations.reverse3( input ), ExpReversed );
  }

  // tests for method isPalindrome

  @Test
  public void testIsPalindrome_tc1() throws Exception {
    String input = "abba";

    Assert.assertTrue ( StringOperations.isPalindrome(input) );
  }

  @Test
  public void testIsPalindrome_tc2() throws Exception {
    String input = "Zagwiżdż i w gaz";

    Assert.assertTrue ( StringOperations.isPalindrome(input) );
  }

  @Test
  public void testIsPalindrome_tc3() throws Exception {
    String input = "not palindrome";

    Assert.assertFalse ( StringOperations.isPalindrome(input) );
  }
}