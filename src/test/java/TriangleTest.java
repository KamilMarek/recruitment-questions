import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class TriangleTest {
  @Test
  public void testEvaluateType_tc1() throws Exception {
    Triangle triangle = new Triangle(1,2,3);

    Assert.assertEquals(triangle.EvaluateType(), TriangleType.NotExisiting);
  }

  @Test
  public void testEvaluateType_tc2() throws Exception {
    Triangle triangle = new Triangle(1,1,3);

    Assert.assertEquals(triangle.EvaluateType(), TriangleType.NotExisiting);
  }

  @Test
  public void testEvaluateType_tc3() throws Exception {
    Triangle triangle = new Triangle(0,1,3);

    Assert.assertEquals(triangle.EvaluateType(), TriangleType.NotExisiting);
  }

  @Test
  public void testEvaluateType_tc4() throws Exception {
    Triangle triangle = new Triangle(3,-1,4);

    Assert.assertEquals(triangle.EvaluateType(), TriangleType.NotExisiting);
  }

  @Test
  public void testEvaluateType_tc5() throws Exception {
    Triangle triangle = new Triangle(0,0,0);

    Assert.assertEquals(triangle.EvaluateType(), TriangleType.NotExisiting);
  }

  @Test
  public void testEvaluateType_tc6() throws Exception {
    Triangle triangle = new Triangle(3,3,5);

    Assert.assertEquals(triangle.EvaluateType(), TriangleType.Isosceles);
  }

  @Test
  public void testEvaluateType_tc7() throws Exception {
    Triangle triangle = new Triangle(3,5,3);

    Assert.assertEquals(triangle.EvaluateType(), TriangleType.Isosceles);
  }

  @Test
  public void testEvaluateType_tc8() throws Exception {
    Triangle triangle = new Triangle(5,3,3);

    Assert.assertEquals(triangle.EvaluateType(), TriangleType.Isosceles);
  }

  @Test
  public void testEvaluateType_tc9() throws Exception {
    Triangle triangle = new Triangle(5,5,5);

    Assert.assertEquals(triangle.EvaluateType(), TriangleType.Equilateral);
  }

  @Test
  public void testEvaluateType_tc10() throws Exception {
    Triangle triangle = new Triangle(3,4,5);

    Assert.assertEquals(triangle.EvaluateType(),TriangleType.Scalene);
  }
}