public class Triangle {
  private int _sideA;
  private int _sideB;
  private int _sideC;

  public Triangle (int sideA, int sideB, int sideC){
    _sideA = sideA;
    _sideB = sideB;
    _sideC = sideC;
  }

  public TriangleType EvaluateType (){
    System.out.println("Triangle " + _sideA + "-" + _sideB + "-"+_sideC);

    if ( !Exists() ){
      return TriangleType.NotExisiting;
    }

    if ( IsEquilateral () ){
      return TriangleType.Equilateral;
    }

    if ( IsIsosceles () ){
      return TriangleType.Isosceles;
    }

    return TriangleType.Scalene;
  }

  private boolean Exists (){
    return AreSidesGreaterThan0 () && IsSumOfTwoLessThanThirdSide ();
  }

  private boolean AreSidesGreaterThan0 (){
    return _sideA > 0 && _sideB > 0 && _sideC > 0;
  }

  private boolean IsSumOfTwoLessThanThirdSide (){
    return _sideA < _sideB + _sideC && _sideB < _sideA + _sideC && _sideC < _sideA + _sideB;
  }

  private boolean IsEquilateral (){
    return _sideA == _sideB && _sideA == _sideC;
  }

  private boolean IsIsosceles (){
    return _sideA == _sideB || _sideA == _sideC || _sideB == _sideC;
  }
}
