import com.sun.org.apache.xpath.internal.operations.Bool;
import com.sun.xml.internal.fastinfoset.util.CharArray;

public class StringOperations {
  public static String reverse ( String input ){

    char[] text = input.toCharArray();

    for ( int left = 0; left < input.length()/2; left++){
      char temp = input.charAt(left);
      int right = input.length()-left-1;
      text[left] = text[right];
      text[right] = temp;
    }

    return new String(text);
  }

  public static String reverse2 ( String input ){

    char[] reversed = new char[input.length()];
    char[] in = input.toCharArray();

    for ( int replaceIndex = 0; replaceIndex < input.length(); replaceIndex++){
      reversed[input.length()-replaceIndex-1] = in[replaceIndex];
    }

    return new String(reversed);
  }

  public static String reverse3 ( String input ){
    return new StringBuilder(input).reverse().toString();
  }

  public static Boolean isPalindrome ( String input ){
    input = input.toLowerCase().replace(" ", "");

    String reversed = reverse3(input);
    System.out.println("input:"+input);
    System.out.println("reversed:"+reversed);

    return input.equals(reversed);
  }
}
